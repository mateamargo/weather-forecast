package forecast.test.weatherforecast.builder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import forecast.test.weatherforecast.core.City;
import forecast.test.weatherforecast.core.Coordinates;
import forecast.test.weatherforecast.core.Forecast;
import forecast.test.weatherforecast.core.Temperature;
import forecast.test.weatherforecast.core.Weather;
import forecast.test.weatherforecast.core.WeatherCondition;

/**
 * Created by mateamargo on 10/10/2015.
 */
public class ForecastBuilder {

    public static Forecast build(String data) throws JSONException {
        Forecast forecast = new Forecast();

        JSONObject obj = new JSONObject(data);

        JSONObject cityJson = obj.getJSONObject("city");
        City city = new City();
        city.setCountry(cityJson.getString("country"));
        city.setName(cityJson.getString("name"));

        JSONObject coordJson = cityJson.getJSONObject("coord");
        Coordinates coord = new Coordinates();
        coord.setLatitude(coordJson.getDouble("lat"));
        coord.setLongitude(coordJson.getDouble("lon"));

        city.setCoordinates(coord);

        forecast.setCity(city);

        JSONArray list = obj.getJSONArray("list");

        for (int i = 0; i < list.length(); i++) {
            JSONObject weatherJson = list.getJSONObject(i);

            Weather weather = new Weather();

            weather.setDate(new Date(weatherJson.getLong("dt") * 1000));
            // Reuse the unique dt value to identify this specific weather later.
            weather.setId(weatherJson.getLong("dt"));
            weather.setPressure(weatherJson.getDouble("pressure"));
            weather.setClouds(weatherJson.getDouble("clouds"));
            weather.setHumidity(weatherJson.getDouble("humidity"));
            weather.setWindDirection(weatherJson.getDouble("deg"));
            weather.setWindSpeed(weatherJson.getDouble("speed"));

            // Siempre viene un solo item.
            JSONObject conditionJson = weatherJson.getJSONArray("weather").getJSONObject(0);
            WeatherCondition condition = new WeatherCondition();
            condition.setDescription(conditionJson.getString("description"));
            condition.setIcon(conditionJson.getString("icon"));
            condition.setMain(conditionJson.getString("main"));
            condition.setId(conditionJson.getLong("id"));

            weather.setCondition(condition);

            JSONObject tempJson = weatherJson.getJSONObject("temp");
            Temperature temp = new Temperature();
            temp.setDay(tempJson.getDouble("day"));
            temp.setMin(tempJson.getDouble("min"));
            temp.setMax(tempJson.getDouble("max"));
            temp.setNight(tempJson.getDouble("night"));
            temp.setMorning(tempJson.getDouble("morn"));
            temp.setEvening(tempJson.getDouble("eve"));

            weather.setTemperature(temp);

            forecast.addWeather(weather);
        }

        return forecast;
    }
}
