package forecast.test.weatherforecast.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa al pronóstico del clima a lo largo de varios días.
 */
public class Forecast {

    public Forecast() {
        weathers = new ArrayList<>();
    }

    private City city;

    private List<Weather> weathers;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Weather> getWeathers() {
        return weathers;
    }

    public void setWeathers(List<Weather> weathers) {
        this.weathers = weathers;
    }

    public void addWeather(Weather weather) {
        weathers.add(weather);
    }
}
