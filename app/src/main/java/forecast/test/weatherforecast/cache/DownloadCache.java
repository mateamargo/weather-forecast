package forecast.test.weatherforecast.cache;

import java.io.OutputStream;
import java.net.URL;

/**
 * Created by mateamargo on 12/10/2015.
 */
public interface DownloadCache {
    OutputStream getFile(String key);

    void putFile(String key, OutputStream data);
}
