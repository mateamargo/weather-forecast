package forecast.test.weatherforecast.cache;

import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mateamargo on 12/10/2015.
 */
public class InMemoryCache implements DownloadCache {

    private static InMemoryCache instance;

    private Map<String, OutputStream> cache = new HashMap<>();

    public static InMemoryCache getInstance() {
        if (instance == null) {
            instance = new InMemoryCache();
        }

        return instance;
    }

    @Override
    public OutputStream getFile(String key) {
        return cache.get(key);
    }

    @Override
    public void putFile(String key, OutputStream data) {
        cache.put(key, data);
    }
}
