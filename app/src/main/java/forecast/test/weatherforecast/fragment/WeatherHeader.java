package forecast.test.weatherforecast.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import forecast.test.weatherforecast.R;
import forecast.test.weatherforecast.core.Forecast;
import forecast.test.weatherforecast.manager.ForecastManager;


public class WeatherHeader extends Fragment {

    private final static String TAG  = "HEADER";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_header, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Forecast forecast = ForecastManager.getInstance().getForecast();

        TextView titleView = (TextView) getView().findViewById(R.id.forecast_header_title);
        titleView.setText(forecast.getCity().getName() + ", " + forecast.getCity().getCountry());

        String lat = String.format("%.2f", forecast.getCity().getCoordinates().getLatitude());
        String lon = String.format("%.2f", forecast.getCity().getCoordinates().getLongitude());

        TextView coordView = (TextView) getView().findViewById(R.id.forecast_city_coord);
        coordView.setText(lat + ", " + lon);
    }
}
