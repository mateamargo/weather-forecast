package forecast.test.weatherforecast.filedownload;

/**
 * Listener que usa el @DownloadFilesTask para notificar sobre el resultado de la descarga.
 */
public interface FileDownloadedListener {

    void onFileDownloadSuccess(Object data);

    void onFileDownloadFailure(Throwable error);
}
