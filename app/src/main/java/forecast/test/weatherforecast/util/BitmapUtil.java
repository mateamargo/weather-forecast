package forecast.test.weatherforecast.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * Utilitario para manejar Bitmaps.
 */
public class BitmapUtil{

    public static Bitmap createBitmap(OutputStream os) {
        ByteArrayOutputStream baos = (ByteArrayOutputStream) os;
        ByteArrayInputStream is = new ByteArrayInputStream(baos.toByteArray());

        return BitmapFactory.decodeStream(is);
    }

}
