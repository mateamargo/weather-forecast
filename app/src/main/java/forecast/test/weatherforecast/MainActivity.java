package forecast.test.weatherforecast;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.net.URL;
import java.util.Properties;

import forecast.test.weatherforecast.manager.ForecastManager;
import forecast.test.weatherforecast.filedownload.DownloadFilesTask;
import forecast.test.weatherforecast.filedownload.FileDownloadedListener;


public class MainActivity extends ActionBarActivity implements FileDownloadedListener  {

    private final static String TAG = "MAIN";
    private final static String CONFIG_FILE = "config.properties";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Properties config = new Properties();
        try {
            config.load(getAssets().open(CONFIG_FILE));

            ForecastManager.getInstance().setIconURL(config.getProperty("icon_url"));

            URL url = new URL(config.getProperty("service_url"));
            new DownloadFilesTask(this).execute(url);

        } catch (Exception e) {
            Log.e(TAG, "Cannot load service", e);
        }

    }

    public void onFileDownloadSuccess(Object data) {
        Intent intnet = new Intent(this, ForecastActivity.class);
        intnet.putExtra("data", data.toString());

        startActivity(intnet);
    }

    public void onFileDownloadFailure(Throwable error) {
        Log.e(TAG, "Cannot download forecast json file", error);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
